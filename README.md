	Am ales sa salvez variabilele ce tin de oficiul maxim , minim , baza global, pentru a nu fi nevoit sa le trimit ca parametri tuturor functiilor (programul initial fusese facut cu structuri , unde transmiterea era ceva mai clara).
    Am initializat distanta minima si pe cea maxima cu limitele gasite in bibilioteca de "float". Iar Raza fiind constanta , am lasat-o cu define.
    Functia "distanta" are formulele de pe site.
    Functia "comparare_afisare" rezolva cerinta problemei pentru fiecare oficiu in parte. De asemenea atualizeaza minimul si maximul ce vor fi afisate la finalul citirii in 'main'.
    In "main" am citit datele de intrare si la fiecare pas am apelat cele 2 functii descrise mai sus , iar la final se vor afisa si minimul si maximul.