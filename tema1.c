#include <stdio.h>
#include <math.h>
#include <float.h> // Pentru limitele lui maxi si mini

#define Rp 6380

static float min_long , min_lat , max_long , max_lat ,
             max_dis = FLT_MIN , min_dis = FLT_MAX;
              // globale pentru finalul problemei

static float d1 , d2 , b_long , b_lat;


float distanta ( float x_lat , float x_long)
{
    float val_1 , val_2 , val_3;

    // Am impartit formula in bucati mai mici pentru ca era prea mare.

    val_1 = sin((b_lat - x_lat)/2) * sin((b_lat - x_lat)/2);
    val_2 = cos (b_lat) * cos (x_lat);
    val_3 = sin((b_long - x_long)/2) * sin((b_long - x_long)/2);

    float x = val_1 + val_2 * val_3;

    return ( 2 * Rp * atan (sqrt(x / (1-x))) );
}

void comparare_afisare ( float x_lat , float x_long )
{
    float dist = distanta (x_lat , x_long);

// gasirea minimului si a maximului de distanta
    if (min_dis > dist)
    {
        min_dis = dist;
        min_lat = x_lat;
        min_long =  x_long;
    }
    if (max_dis < dist)
    {
        max_dis = dist;
        max_lat = x_lat;
        max_long =  x_long;
    }

    if ( dist < d1 )
    {
        printf ("<%.4f, %.4f> VECIN\n" , x_lat , x_long);
        return ;
    }

    if ( dist < d2 )
    {
        printf ("<%.4f, %.4f> APROPIAT\n" , x_lat , x_long);
        return ;
    }

    printf ("<%.4f, %.4f> DEPARTAT\n" , x_lat , x_long);
}

int main ()
{
    int n , i;
    float x_lat , x_long;

    scanf ("%d%f%f" , &n , &d1 , &d2);
    scanf ("%f%f" , &b_lat , &b_long); // Citire coordonate baza

    for (i=0 ; i<n ; i++)
    {
        scanf ("%f%f" , &x_lat , &x_long);

        comparare_afisare (x_lat , x_long);
    }

    printf ("CEL MAI APROPIAT: <%.4f, %.4f>\n" ,  min_lat , min_long);
    printf ("CEL MAI DEPARTAT: <%.4f, %.4f>\n" ,  max_lat , max_long);

    return 0;
}
